from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(attrs={"placeholder": "Password"}),
    )

    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(attrs={"placeholder": "Confirm Password"}),
    )
